// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


const develope_branch = {
  baseURI: 'https://webconnect.thankucash.dev',
  PayStackKey: "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6"
}

const quality_asurance_branch = {
  baseURI: 'https://webconnect.thankucash.tech',
  PayStackKey: "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6"
}

const staging_branch = {
  baseURI: 'https://webconnect.thankucash.co',
  PayStackKey: "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6"
}

const production_branch = {
  baseURI: 'https://webconnect.thankucash.com',
  PayStackKey: "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6"
}

export const environment = {
  production: false,
  appSettings: config(),
};

function config(): any {
  const isLocal = (window.location.host as string).includes('localhost');
  const host = isLocal ? 'localhost' : window.location.host;
  if (host == 'localhost') return develope_branch;
  if (host == 'giftcard.thankucash.dev') return develope_branch;
  if (host == 'giftcard.thankucash.tech') return quality_asurance_branch;
  if (host == 'giftcard.thankucash.co') return staging_branch;
  if (host == 'giftcard.thankucash.com') return production_branch;
  return {};
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
