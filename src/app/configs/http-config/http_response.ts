export class OResponse {
    public Status: string;
    public Message: string;
    public ResponseCode: string;
    public Result: any;
}

export interface ONetworkResponse {
    zx: string;
    fx: string;
    vx: string;
}

export class ClientHeader {
    "Content-Type": string = 'application/json; charset=utf-8';
    hcak: string = btoa("33f51da41c95415fba82ff8a9f4981a5");
    hcavk: string = btoa("18da648a59024f2ba93e2e07319b1764");
    hctk: string = btoa("na");
    hcudlt: string = btoa("na");
    hcudln: string = btoa("na");
    hcuak: string = "";
    hcupk: string = btoa("na");
    hcudk: string = btoa("na");
    hcuata: string = "";
    hcuatp: string = "";
    // Authorization: string = btoa("18da648a59024f2ba93e2e07319b1764");

    constructor(hctk: any) {
        this.hctk = btoa(hctk);
    }
}