import { NotifyService } from './../../shared/notify.service';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ONetworkResponse, OResponse, ClientHeader } from './http_response';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient,
    private notify: NotifyService
  ) { }

  get(obj: any): Observable<any> {
    const url = obj.url;
    return this.http.get<any>(url).pipe(
      map(response => { return this.extractResponse(response); }),
      catchError(e => this._error(e))
    );
  }

  post(obj: any): Observable<any> {
    const params = obj.params;
    const url = obj.url;
    const postData = obj.postData;
    let headers: any = new ClientHeader(postData.Task) as ClientHeader;
    if (obj.isAuth) {
      delete headers.hcak;
      delete headers.hcavk;
    }
    let _Headers = new HttpHeaders(headers);
    var ORequest = {
      zx: btoa(unescape(encodeURIComponent(JSON.stringify(postData)))),
    };

    var RequestData = JSON.stringify(ORequest);
    return this.http.post<ONetworkResponse>(url, RequestData, { headers: _Headers, params }).pipe(
      map(response => { return this.extractResponse(response, obj.is_toast); }),
      catchError(e => this._error(e))
    );
  }

  private extractResponse(res: any, isToast: boolean = true) {
    const data = (res.zx ? JSON.parse(atob(res.zx)) : res) as OResponse;
    const statuses = ['Success', 'success', 200, '200'];
    if (statuses.includes(data.Status)) {
      if (isToast) this.notify.NotifySuccess(data.Message);
      return data;
    }
    else {
      this.notify.NotifyError(data.Message);
      return data;
    }
  }

  private _error(error: HttpErrorResponse): any {
    if(error.status == 0){
      this.notify.NotifyError("Server failed and not available. Please try again.");
      return throwError("Server failed and not available. Please try again.");
    }
    const data = (error.error.zx ? JSON.parse(atob(error.error.zx)) : error.error) as OResponse;
    if (data) {
      this.notify.NotifyError(data.Message);
      return throwError(data.Message);
    } else {
      if(error.message){
        this.notify.NotifyError(error.message);
      }else {
        this.notify.NotifyError("Server issue. Please try again.");
      }
      return throwError(error.message);
    }
  }


}
