import { MultipleReceiversInfoComponent } from './../../pages/multiple-receivers-info/multiple-receivers-info.component';
import { Route, Routes } from "@angular/router";
import { GiftCardCheckoutComponent } from "src/app/pages/gift-card-checkout/gift-card-checkout.component";
import { GiftCardDetailsComponent } from "src/app/pages/gift-card-details/gift-card-details.component";
import { GiftCardDownloadComponent } from "src/app/pages/gift-card-download/gift-card-download.component";
import { GiftCardUploadComponent } from "src/app/pages/gift-card-upload/gift-card-upload.component";
import { PaymentSuccessComponent } from "src/app/pages/payment-success/payment-success.component";
import { ReceivierInfoComponent } from "src/app/pages/receivier-info/receivier-info.component";
import { SendersInfoComponent } from "src/app/pages/senders-info/senders-info.component";

export const GiftCardRouting: Routes = [
    {
        path: '',
        redirectTo: 'giftcarddetails',
        pathMatch: 'full'
    },
    {
        path: 'giftcarddetails',
        component: GiftCardDetailsComponent
    },
    {
        path: 'receiver',
        component: ReceivierInfoComponent
    },
    {
        path: 'receivers',
        component: MultipleReceiversInfoComponent
    },
    {
        path: 'sender',
        component: SendersInfoComponent
    },
    {
        path: 'giftcarddownload',
        component: GiftCardDownloadComponent
    },
    {
        path: 'giftcardupload',
        component: GiftCardUploadComponent
    },
    {
        path: 'checkout',
        component: GiftCardCheckoutComponent
    },
    {
        path: 'success',
        component: PaymentSuccessComponent
    }
];