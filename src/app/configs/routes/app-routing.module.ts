import { GiftCardComponent } from '../../pages/gift-card/gift-card.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GiftCardRouting } from './giftcards.routing';
import { BalanceCheckCardComponent } from 'src/app/pages/balance-check/balance-check-card/balance-check-card.component';

const routes: Routes = [
  {
    path: '',
    component: GiftCardComponent
  },
  {
    path: 'individual',
    data: {
      activateTemplate: 'individual'
    },
    children: GiftCardRouting,
  },
  {
    path: 'corporate',
    data: {
      activateTemplate: 'corporate'
    },
    children: GiftCardRouting
  },
  {
    path: 'checkbalance',
    component: BalanceCheckCardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
