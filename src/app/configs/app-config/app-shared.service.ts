import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router, RoutesRecognized } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppSharedService {

  private route_data: any;

  constructor(
    private titleService: Title,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.router.events.subscribe(e => {
      if (e instanceof RoutesRecognized) {
        this.router.events.pipe(filter(event => event instanceof NavigationEnd),
          map(() => this.route.snapshot),
          map(route => {
            while (route.firstChild) {
              route = route.firstChild;
            }
            return route;
          })).subscribe((route: ActivatedRouteSnapshot) => {
            this.route_data = route.data;
            if (this.route_data['activateTemplate']) {
              this.activateTemplate = this.route_data['activateTemplate'];
            } else this.activateTemplate = '';
            if (this.route_data['title']) {
              var title = this.route_data['title'];
              this.titleService.setTitle(title);
            }
          });
      }
    });
  }

  static isLoading: boolean = false;
  get isLoading(): boolean { return AppSharedService.isLoading; }
  set isLoading(val: boolean) { AppSharedService.isLoading = val; }

  static activateTemplate: string = '';
  get activateTemplate(): string { return AppSharedService.activateTemplate; }
  set activateTemplate(val: string) { AppSharedService.activateTemplate = val; }

  async delay(time: number): Promise<any> {
    await new Promise(resolve => setTimeout(resolve, time));
  }

  async download(file: File): Promise<void> {
    const a = document.createElement('a')
    const objectUrl = URL.createObjectURL(file)
    a.href = objectUrl
    a.download = file.name;
    a.click();
    URL.revokeObjectURL(objectUrl);
  }

  scrollToView(id: string): void {
    const data = document.getElementById(id);
    if (data) {
      data.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
    }
  }

  lowerCaseConvert(str){   
    let email_lowercase = "";
    for (let chars of str) {
        //Get the ascii value of character
        let value = chars.charCodeAt();

        //If the character is in uppercase
        if (value >= 65 && value <= 90) {
            //convert it to lowercase
            email_lowercase += String.fromCharCode(value + 32);
        } else {
            //else add the original character
            email_lowercase += chars;
        }
    }
    return email_lowercase
}

}