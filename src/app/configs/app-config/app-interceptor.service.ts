import { AppSharedService } from './app-shared.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AppInterceptor implements HttpInterceptor {

    private pendingReqCnt: number = 0;

    static forceStop: boolean = false;
    get forceStop(): boolean { return AppInterceptor.forceStop; }
    set forceStop(val: boolean) { AppInterceptor.forceStop = val; }

    static filterUrls: string[] = [];
    get filterUrls(): string[] { return AppInterceptor.filterUrls; }
    set filterUrls(val: string[]) { AppInterceptor.filterUrls = val; }

    static filterUrlPatterns: RegExp[] = [];
    get filterUrlPatterns(): RegExp[] { return AppInterceptor.filterUrlPatterns; }
    set filterUrlPatterns(val: RegExp[]) { AppInterceptor.filterUrlPatterns = val; }

    private checkIsFilterUrl(url: string): boolean {
        return this.filterUrls.some(x => x === url)
    }

    private checkUrlPatterns(url: string): boolean {
        return this.filterUrlPatterns.some(x => x.test(url));
    }

    private isStopReq(r: HttpRequest<any>): boolean {
        return this.forceStop
            || this.checkIsFilterUrl(r.urlWithParams)
            || this.checkUrlPatterns(r.urlWithParams)
    }

    constructor(private shared: AppSharedService) { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {
        const isNext = this.isStopReq(request);
        if (!isNext) {
            this.pendingReqCnt++;
            if (1 === this.pendingReqCnt) {
                this.shared.isLoading = true;
            }
        }
        return next.handle(request).pipe(finalize(() => {
            if (!isNext) {
                this.pendingReqCnt--;
                if (0 === this.pendingReqCnt) {
                    this.shared.isLoading = false;
                }
            }
        }));
    }
}
