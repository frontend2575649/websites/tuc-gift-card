import { GiftCardService } from './shared/gift-card.service';
import { GiftCardComponent } from 'src/app/pages/gift-card/gift-card.component';
import { ComponentsModule } from './components/components.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './configs/routes/app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { FooterComponent } from './core/footer/footer.component';
import { Angular4PaystackModule } from 'angular4-paystack';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GiftCardCheckoutComponent } from 'src/app/pages/gift-card-checkout/gift-card-checkout.component';
import { GiftCardDetailsComponent } from 'src/app/pages/gift-card-details/gift-card-details.component';
import { GiftCardDownloadComponent } from 'src/app/pages/gift-card-download/gift-card-download.component';
import { GiftCardUploadComponent } from 'src/app/pages/gift-card-upload/gift-card-upload.component';
import { ReceivierInfoComponent } from 'src/app/pages/receivier-info/receivier-info.component';
import { SendersInfoComponent } from 'src/app/pages/senders-info/senders-info.component';
import { AppSharedService } from './configs/app-config/app-shared.service';
import { MatDialogModule } from '@angular/material/dialog';
import { PaymentSuccessComponent } from './pages/payment-success/payment-success.component';
import { NotifyService } from './shared/notify.service';
import { HttpConfigModule } from './configs/http-config/http-config.module';
import { SearchMultiselectDropdownModule } from 'search-multiselect-dropdown';
import { CKEditorModule } from 'ckeditor4-angular';
import { MultipleReceiversInfoComponent } from './pages/multiple-receivers-info/multiple-receivers-info.component';
import { BalanceCheckCardComponent } from './pages/balance-check/balance-check-card/balance-check-card.component';
import { BalanceCheckInputComponent } from './pages/balance-check/balance-check-input/balance-check-input.component';
import { ShowBalanceComponent } from './pages/balance-check/show-balance/show-balance.component';
import { CodeInputModule } from 'angular-code-input';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    GiftCardComponent,
    GiftCardCheckoutComponent,
    GiftCardDetailsComponent,
    GiftCardDownloadComponent,
    GiftCardUploadComponent,
    ReceivierInfoComponent,
    SendersInfoComponent,
    PaymentSuccessComponent,
    MultipleReceiversInfoComponent,
    BalanceCheckCardComponent,
    BalanceCheckInputComponent,
    ShowBalanceComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ComponentsModule,
    Angular4PaystackModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    HttpConfigModule,
    SearchMultiselectDropdownModule,
    CKEditorModule,
    CodeInputModule
  ],
  providers: [
    AppSharedService,
    NotifyService,
    GiftCardService
  ],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule {
  constructor(private shared: AppSharedService) { }
}
