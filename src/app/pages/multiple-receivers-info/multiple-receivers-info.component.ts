import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppSharedService } from 'src/app/configs/app-config/app-shared.service';
import { CKEditorConfig, GiftCard } from 'src/app/shared/gift_card';
import { NotifyService } from 'src/app/shared/notify.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-multiple-receivers-info',
  templateUrl: './multiple-receivers-info.component.html',
  styleUrls: ['./multiple-receivers-info.component.scss']
})
export class MultipleReceiversInfoComponent implements OnInit {

  reactiveForm!: FormGroup;
  amount: number = 0;
  phoneData: any;
  total_users: number = 0;
  remaining_amount: number = 0;
  minimum_amount: number = 0;
  users_Data: any[] = [];
  config = CKEditorConfig;
  updateIndex: any = null;
  is_equaly_divide: boolean = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private shared: AppSharedService,
    private cdr: ChangeDetectorRef,
    private notify: NotifyService,
  ) {
    this.reactiveForm = this.fb.group({
      name: ['', Validators.required],
      amount: ['', [Validators.required]],
      email: [null, Validators.compose([Validators.nullValidator, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
      // phone: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14), Validators.pattern("^((\\+243-?)|0)?[0-9]{10,14}$")])],
      phone: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      desc: ['', Validators.nullValidator],
    });
  }

  ngAfterViewInit(): void {
    this.shared.scrollToView('scrollToCardDetails');
  }

  ngOnInit(): void {
    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data && data.receiverInfo && data.receiverInfo.length > 0) {
      this.users_Data = data.receiverInfo.map(x => {
        return {
          Name: x.Name,
          Email: x.Email,
          MobileNo: x.MobileNo,
          CountryIsd: x.CountryIsd,
          Amount: x.Amount,
          Description: x.Description,
          show: false
        }
      });
    }
    if (data) {
      this.amount = Number(data.amount);
      this.remaining_amount = this.amount;
      this.total_users = data.no_receivers;
      this.is_equaly_divide = data.is_equaly_divide;
      this.updateAmountValidation();
      if (this.is_equaly_divide) {
        this.f.amount.patchValue(this.divideEqualAmount());
      }
    }
  }

  updateAmountValidation(): void {
    this.minimum_amount = 1;
    if (this.remaining_amount <= 1) {
      this.minimum_amount = this.remaining_amount;
    }
    this.updateValidator('amount', [Validators.required, Validators.min(this.minimum_amount), Validators.max(this.remaining_amount)], '');
  }

  updateValidator(input: string, validators: any[], value: string): void {
    this.reactiveForm.get(input)!.setValidators(validators);
    this.reactiveForm.get(input)!.setValue(value);
    this.reactiveForm.get(input)!.updateValueAndValidity();
  }

  get f(): any {
    return this.reactiveForm.controls;
  }

  divideEqualAmount(): any {
    const n: number = (this.amount / this.total_users);
    if (Number(n) === n && n % 1 !== 0) {
      // return n.toFixed(2);
      return Math.round(n);
    }
    return n;
  }

  onBlur(): void {
    this.reactiveForm.get('phone').markAsTouched();
  }

  async save(): Promise<void> {
    await this.shared.delay(200);
    if (this.reactiveForm.invalid) {
      for (var i in this.f) {
        this.f[i].markAsTouched();
      }
      return;
    }
    const receiver = {
      Name: this.f.name.value,
      Email: this.shared.lowerCaseConvert(this.f.email.value),
      MobileNo: this.phoneData.nationalNumber.replace(/\s+/g, ''),
      CountryIsd: this.phoneData.dialCode,
      Amount: Number(this.f.amount.value),
      Description: this.f.desc.value,
      show: false,
      ishide: false
    }
    if (typeof (this.updateIndex) == 'number' && this.updateIndex >= 0) {
      this.users_Data[this.updateIndex] = receiver;
    } else {
      this.users_Data.push(receiver);
    }
    this.remaining_amount = Number((this.amount - this.users_Data.reduce((partialSum, x) => partialSum + x.Amount, 0)).toFixed(2));
    this.reactiveForm.reset();
    this.phoneData = null;
    this.updateIndex = null;
    this.updateAmountValidation();
    if (this.is_equaly_divide && this.remaining_amount > 0) {
      if (this.users_Data.length == this.total_users - 1)
        this.f.amount.patchValue(this.remaining_amount);
      else this.f.amount.patchValue(this.divideEqualAmount());
    }
  }

  showDesc(index): void {
    if (this.users_Data[index].show) {
      this.users_Data[index].show = false;
    } else {
      this.users_Data.map(x => x.show = false);
      this.users_Data[index].show = true;
    }
  }

  openEdit(item, index): void {
    this.users_Data.map(x => x['ishide'] = false);
    this.remaining_amount = Number((this.amount - this.users_Data.reduce((partialSum, x) => partialSum + x.Amount, 0)).toFixed(2));
    this.remaining_amount += item.Amount;
    this.updateAmountValidation();
    this.updateIndex = index;
    this.f.name.patchValue(item.Name);
    this.f.email.patchValue(item.Email);
    this.f.phone.patchValue([("+" + item.CountryIsd), item.MobileNo].join(''));
    this.f.desc.patchValue(item.Description);
    this.f.amount.patchValue(item.Amount);
    item['ishide'] = true;
  }

  back() {
    this.router.navigateByUrl('/individual/giftcarddetails');
  }

  async goToSender(): Promise<void> {
    if (this.isPreviewDisabled()) {
      // this.notify.NotifyError("Entered amount is not matching with total amount");
      const swal = await Swal.fire({
        text: 'Entered amount is not matching with receivers total amount.',
        confirmButtonText: 'Ok',
        confirmButtonColor: '#562A83',
        customClass: 'custom-swal',
      });
      return;
    }
    const receviers = this.users_Data.map(x => {
      return {
        Name: x.Name,
        Email: x.Email,
        MobileNo: x.MobileNo,
        CountryIsd: x.CountryIsd,
        Amount: x.Amount,
        Description: x.Description
      }
    });

    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data) {
      data.receiverInfo = receviers;
      localStorage.setItem('tucgiftcarddetails', JSON.stringify(data));
    }
    this.router.navigateByUrl('/individual/sender');
  }

  isPreviewDisabled(): boolean {
    this.remaining_amount = Number((this.amount - this.users_Data.reduce((partialSum, x) => partialSum + x.Amount, 0)).toFixed(2));
    if (this.remaining_amount > this.amount || this.remaining_amount > 0) {
      // this.notify.NotifyError("Entered amount is not matching with total amount");
      return true;
    }

    return false;
  }
}