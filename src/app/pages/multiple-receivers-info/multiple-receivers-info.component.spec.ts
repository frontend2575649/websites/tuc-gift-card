import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleReceiversInfoComponent } from './multiple-receivers-info.component';

describe('MultipleReceiversInfoComponent', () => {
  let component: MultipleReceiversInfoComponent;
  let fixture: ComponentFixture<MultipleReceiversInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleReceiversInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleReceiversInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
