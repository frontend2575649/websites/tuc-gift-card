import { GiftCard, GiftCardTypes, Selectconfig } from './../../shared/gift_card';
import { AppSharedService } from './../../configs/app-config/app-shared.service';
import { Router } from '@angular/router';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DropDownConfig } from 'search-multiselect-dropdown';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-gift-card-details',
  templateUrl: './gift-card-details.component.html',
  styleUrls: ['./gift-card-details.component.scss']
})
export class GiftCardDetailsComponent implements OnInit, AfterViewInit {

  reactiveForm!: FormGroup;
  config = Selectconfig;
  options = GiftCardTypes;
  constructor(
    private router: Router,
    public shared: AppSharedService,
    private fb: FormBuilder,
  ) {
    this.reactiveForm = this.fb.group({
      company_name: ['', Validators.required],
      amount: ['', [Validators.required, Validators.min(1000)]],
      type: ['', Validators.required],
      no_receivers: ['', Validators.nullValidator],
    });

  }

  ngAfterViewInit(): void {
    this.shared.scrollToView('scrollToCardDetails');
  }

  get f(): any {
    return this.reactiveForm.controls;
  }

  ngOnInit(): void {
    if (this.shared.activateTemplate == 'individual') {
      this.updateValidator('company_name', [Validators.nullValidator], '');
      this.updateValidator('type', [Validators.required], '');
    } else if (this.shared.activateTemplate == 'corporate') {
      this.updateValidator('company_name', [Validators.required], '');
      this.updateValidator('type', [Validators.required], '1');
    }

    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data) {
      if (data.amount) this.f.amount.value = data.amount;
      if (data.company_name) this.f.company_name.value = data.company_name;
      if (data.type) this.f.type.value = this.options.find(x => x.id == data.type);
      this.onChangeSelect(this.f.type);
      if (data.no_receivers) this.f.no_receivers.value = data.no_receivers;
    }

  }

  onChangeSelect($event): void {
    if ($event && $event.value && $event.value.id == 1 && this.shared.activateTemplate == 'individual') {
      this.updateValidator('no_receivers', [Validators.required], '');
    } else {
      this.updateValidator('no_receivers', [Validators.nullValidator], '');
    }
  }

  updateValidator(input: string, validators: any[], value: string): void {
    this.reactiveForm.get(input)!.setValidators(validators);
    this.reactiveForm.get(input)!.setValue(value);
    this.reactiveForm.get(input)!.updateValueAndValidity();
  }

  async save(): Promise<void> {
    if (this.reactiveForm.invalid) {
      for (var i in this.f) {
        this.f[i].markAsTouched();
      }
      return;
    }

    let is_equaly_divide = false;

    if (this.shared.activateTemplate == 'individual' && this.f.type.value.id == '1' && parseInt(this.f.no_receivers.value) < 6) {
      const swal = await Swal.fire({
        text: 'Divide Giftcard amount equally among receivers?',
        showCancelButton: true,
        confirmButtonColor: '#562A83',
        confirmButtonText: 'Yes, please do',
        cancelButtonText: 'No',
        customClass: 'custom-swal',
      });
      if (swal.value === true) {
        is_equaly_divide = true;
      }

    }


    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data) {
      data.amount = this.f.amount.value;
      data.company_name = this.f.company_name.value;
      data.type = this.shared.activateTemplate == 'corporate' ? 1 : this.f.type.value.id;
      data.no_receivers = this.f.no_receivers.value ? parseInt(this.f.no_receivers.value) : 0;
      data.is_equaly_divide = is_equaly_divide;
      data.receiverInfo = [];
      localStorage.setItem('tucgiftcarddetails', JSON.stringify(data));
    }

    if (this.shared.activateTemplate == 'individual') {
      if (data.type == '1')
        if (data.no_receivers > 5)
          this.router.navigateByUrl('/individual/giftcarddownload')
        else {
          this.router.navigateByUrl('/individual/receivers')
        }
      else
        this.router.navigateByUrl('/individual/receiver')
    }

    if (this.shared.activateTemplate == 'corporate') {
      this.router.navigateByUrl('/corporate/giftcarddownload')
    }

  }
}
