import { NotifyService } from './../../shared/notify.service';
import { AfterContentInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppSharedService } from 'src/app/configs/app-config/app-shared.service';
import * as XLSX from 'xlsx';
import { GiftCard } from 'src/app/shared/gift_card';
import * as lpn from 'google-libphonenumber';
import * as utils from '../../../../node_modules/intl-tel-input/build/js/utils.js';

@Component({
  selector: 'app-gift-card-upload',
  templateUrl: './gift-card-upload.component.html',
  styleUrls: ['./gift-card-upload.component.scss']
})
export class GiftCardUploadComponent implements OnInit, AfterContentInit {
  @ViewChild('uploadInputFile') uploadInputFile: ElementRef;
  file: any;
  arrayBuffer: any;
  filelist: any;
  amount = 0;
  isLoading: boolean = false;
  isValid: boolean = false;
  constructor(
    private router: Router,
    private notify: NotifyService,
    private shared: AppSharedService
  ) {
  }

  ngAfterContentInit(): void {
    this.shared.scrollToView('scrollToCardDetails');
  }

  ngOnInit(): void {
    this.isValid = false;
    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data) {
      this.amount = Number(data.amount);
      data.document_file = null;
      data.receiverInfo = [];
      localStorage.setItem('tucgiftcarddetails', JSON.stringify(data));
    }
  }

  uploadFile($event: any): void {
    this.isLoading = true;
    this.isValid = false;
    this.file = $event[0] ? $event[0] : $event.target.files[0];
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = async (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      try {
        var workbook = XLSX.read(bstr, { type: "binary" });
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        var arraylist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
        let fileInvalid = false
        let totalAmt = 0
        let receiverInfo = [];
        for (let index = 0; index < arraylist.length; index++) {
          const item = arraylist[index];
          if (item["Enter Receiver's Email"] && item["Enter Amount"] && item["Enter Receiver's Country Code"] && item["Enter Receiver's Phone Number"] && item["Enter Receiver's First Name"]) {
            // try {
            //   var phoneUtil = lpn.PhoneNumberUtil.getInstance();
            //   if (!phoneUtil.isValidNumber(phoneUtil.parseAndKeepRawInput(this.parsePhone(item["Enter Receiver's Country Code"], item["Enter Receiver's Phone Number"])))) {
            //     fileInvalid = true;
            //     this.notify.NotifyError("This " + this.parsePhone(item["Enter Receiver's Country Code"], item["Enter Receiver's Phone Number"]) + " phone number is invalid.");
            //     this.isLoading = false;
            //     this.uploadInputFile.nativeElement.value = "";
            //     return;
            //   }
            // } catch (error) {
            //   this.notify.NotifyError("This " + this.parsePhone(item["Enter Receiver's Country Code"], item["Enter Receiver's Phone Number"]) + " phone number is invalid.");
            //   this.isLoading = false;
            //   this.uploadInputFile.nativeElement.value = "";
            //   return;
            // }
            let receiverObj = {
              "Name": [item["Enter Receiver's First Name"], item["Enter Receiver's Last Name"]].filter(x => x).join(' '),
              "Email": item["Enter Receiver's Email"],
              "CountryIsd": this.parseCountryCode(item["Enter Receiver's Country Code"]),
              "MobileNo": item["Enter Receiver's Phone Number"],
              "Amount": item["Enter Amount"],
              "Description": item["Personalized Message"] ? item["Personalized Message"] : ""
            }
            receiverInfo.push(receiverObj)
            totalAmt = totalAmt + item["Enter Amount"];
          } else if (!item["Enter Receiver's Country Code"]) {
            this.notify.NotifyError("Country code is missing for phone number.");
            this.isLoading = false;
            this.uploadInputFile.nativeElement.value = "";
            return;
          }
          else {
            fileInvalid = true;
          }
        }
        if (fileInvalid) {
          this.notify.NotifyError("Invalid file content found. Please upload proper content in excel file.");
        }
        else if (totalAmt != this.amount) {
          this.notify.NotifyError("Entered amount is not matching with total amount")
        }
        else {
          const toBase64 = file => new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
          });
          this.isValid = true;
          let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
          if (data) {
            data.receiverInfo = receiverInfo;
            data.document_file = await toBase64(this.file);
            localStorage.setItem('tucgiftcarddetails', JSON.stringify(data));
          }

          // this.goToPreview();
        }
        this.isLoading = false;
        this.uploadInputFile.nativeElement.value = "";
      } catch (e) {
        console.log(e);
        this.notify.NotifyError("Invalid file. Please upload only excel file.");
        this.isLoading = false;
        this.uploadInputFile.nativeElement.value = "";
      }
    }
    fileReader.onerror = (e) => {
      this.isLoading = false;
      this.uploadInputFile.nativeElement.value = "";
    }
  }
  parsePhone(code, phone): any {
    if (code.toString().indexOf('+') === -1) code = '+' + code.toString();
    return code + phone;
  }

  parseCountryCode(code: string): string {
    if (code.toString().indexOf('+') > -1) return code.toString().replace('+', "");
    return code;
  }

  goToPreview(): void {
    if (this.isValid) {
      if (this.shared.activateTemplate == 'corporate') {
        this.router.navigateByUrl('/corporate/sender')
      }
      else this.router.navigateByUrl('/individual/sender');
    }
  }

  back(): void {
    if (this.shared.activateTemplate == 'corporate') {
      this.router.navigateByUrl('/corporate/giftcarddownload')
    }
    else this.router.navigateByUrl('/individual/giftcarddownload');
  }
} 