import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftCardUploadComponent } from './gift-card-upload.component';

describe('GiftCardUploadComponent', () => {
  let component: GiftCardUploadComponent;
  let fixture: ComponentFixture<GiftCardUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GiftCardUploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCardUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
