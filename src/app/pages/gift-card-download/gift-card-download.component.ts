import { Router } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppSharedService } from 'src/app/configs/app-config/app-shared.service';
import { GiftCard } from 'src/app/shared/gift_card';

@Component({
  selector: 'app-gift-card-download',
  templateUrl: './gift-card-download.component.html',
  styleUrls: ['./gift-card-download.component.scss']
})
export class GiftCardDownloadComponent implements OnInit, AfterViewInit {

  amount: number = 0;
  constructor(
    private router: Router,
    public shared: AppSharedService,
  ) {

  }

  ngAfterViewInit(): void {
    this.shared.scrollToView('scrollToCardDetails');
  }

  ngOnInit(): void {
    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data) {
      this.amount = Number(data.amount);
    }
  }

  goToUpload(): void {
    if (this.shared.activateTemplate == 'corporate') {
      this.router.navigateByUrl('/corporate/giftcardupload')
    }
    else this.router.navigateByUrl('/individual/giftcardupload');
  }

  back(): void {
    if (this.shared.activateTemplate == 'corporate') {
      this.router.navigateByUrl('/corporate/giftcarddetails')
    }
    else this.router.navigateByUrl('/individual/giftcarddetails');
  }
}
