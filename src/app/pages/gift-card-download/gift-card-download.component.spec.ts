import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftCardDownloadComponent } from './gift-card-download.component';

describe('GiftCardDownloadComponent', () => {
  let component: GiftCardDownloadComponent;
  let fixture: ComponentFixture<GiftCardDownloadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GiftCardDownloadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCardDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
