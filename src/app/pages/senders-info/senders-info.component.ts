import { AppSharedService } from 'src/app/configs/app-config/app-shared.service';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CKEditorConfig, GiftCard } from 'src/app/shared/gift_card';
import { GiftCardService } from 'src/app/shared/gift-card.service';

@Component({
  selector: 'app-senders-info',
  templateUrl: './senders-info.component.html',
  styleUrls: ['./senders-info.component.scss']
})
export class SendersInfoComponent implements OnInit, AfterViewInit {

  reactiveForm!: FormGroup;
  model: GiftCard;
  phoneData: any;
  config = CKEditorConfig;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private shared: AppSharedService,
    private service: GiftCardService,
  ) {
    this.reactiveForm = this.fb.group({
      name: ['', Validators.required],
      desc: ['', Validators.nullValidator],
      email: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
      // phone: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14), Validators.pattern("^((\\+243-?)|0)?[0-9]{10,14}$")])],
      phone: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
    });
  }
  ngAfterViewInit(): void {
    this.shared.scrollToView('scrollToCardDetails');
  }

  ngOnInit(): void {
    this.model = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (this.model && this.model.senderInfo) {
      this.f.name.value = this.model.senderInfo.Name;
      this.f.email.value = this.model.senderInfo.Email;
      this.f.phone.value = [("+" + this.model.senderInfo.CountryIsd), this.model.senderInfo.MobileNo].join('');
      this.f.desc.value = this.model.senderInfo.Description;
    }
  }

  get f(): any {
    return this.reactiveForm.controls;
  }

  onBlur(): void {
    this.reactiveForm.get('phone').markAsTouched();
  }

  save(): void {
    if (this.reactiveForm.invalid) {
      for (var i in this.f) {
        this.f[i].markAsTouched();
      }
      return;
    }
    const postdata = {
      Name: this.f.name.value,
      Email: this.shared.lowerCaseConvert(this.f.email.value),
      MobileNo: this.phoneData.nationalNumber.replace(/\s+/g, ''),
      Description: this.f.desc.value,
      CountryIsd: this.phoneData.dialCode,
    }
    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    data.senderInfo = { ...postdata };
    this.model = data
    localStorage.setItem('tucgiftcarddetails', JSON.stringify(data));
    this.checkoutPayment();


  }

  back(): void {
    if (this.shared.activateTemplate == 'corporate') {
      this.router.navigateByUrl('/corporate/giftcardupload')
    }
    else {
      if (this.model.type == '1' && this.model.no_receivers < 6) {
        this.router.navigateByUrl(`/${this.model!.main_type}/receivers`);
      }
      else if (this.model.type == '1') {
        this.router.navigateByUrl('/individual/giftcardupload');
      } else {
        this.router.navigateByUrl('/individual/receiver');
      }
    }
  }

  checkoutDisabled: boolean = false;

  checkoutPayment() {
    this.checkoutDisabled = true;
    let start = new Date();
    let StartDate = new Date().toISOString().split('T')[0] + ' 00:00:00';
    let endDate = [start.getFullYear() + 1, (start.getMonth() < 10 ? '0' : '') + start.getMonth(), start.getDate()].join('-') + ' 00:00:00';
    var postdata = {
      Task: 'savegiftcarduserdrafts',
      Amount: this.model.amount,
      isIndividualorCorporate: this.model.main_type == "individual" ? 1 : 0, //0:Co-orporate,1:Individual
      type: this.model.type == "1" ? 1 : 0,  //0: single, 1:multiple
      senderInfo: this.model.senderInfo,
      receiverInfo: this.model.receiverInfo,
      StartDate: StartDate,
      EndDate: endDate,
      PaymentSource: "PayStack",
      CompanyName: this.model.company_name
    }
    this.service.saveGiftDraft(postdata.Task, postdata).subscribe(res => {
      this.checkoutDisabled = false;
      if (res && res.Result && res.Result.Commission) {
        this.model.commission = res.Result.Commission;
        localStorage.setItem('tucgiftcarddetails', JSON.stringify(this.model));
        if (this.shared.activateTemplate == 'corporate') {
          this.router.navigateByUrl('/corporate/checkout')
        }
        else this.router.navigateByUrl('/individual/checkout');
      }
    }, err => {
      this.checkoutDisabled = false;
    })
  }
}
