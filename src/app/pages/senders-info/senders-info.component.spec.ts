import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendersInfoComponent } from './senders-info.component';

describe('SendersInfoComponent', () => {
  let component: SendersInfoComponent;
  let fixture: ComponentFixture<SendersInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendersInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendersInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
