import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceCheckInputComponent } from './balance-check-input.component';

describe('BalanceCheckInputComponent', () => {
  let component: BalanceCheckInputComponent;
  let fixture: ComponentFixture<BalanceCheckInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceCheckInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceCheckInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
