import { AppSharedService } from './../../../configs/app-config/app-shared.service';
import { AbstractControl, FormBuilder, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { BalancePayload } from 'src/app/shared/balance-check';
import { CodeInputComponent } from 'angular-code-input';
import { GiftCardService } from 'src/app/shared/gift-card.service';

@Component({
  selector: 'app-balance-check-input',
  templateUrl: './balance-check-input.component.html',
  styleUrls: ['./balance-check-input.component.scss']
})
export class BalanceCheckInputComponent implements OnInit {

  model: BalancePayload;
  @ViewChild('codeInput') codeInput !: CodeInputComponent;
  isLoader: boolean = false;
  @Output() onGetData: EventEmitter<any> = new EventEmitter();
  @Output() phonenumber: EventEmitter<any> = new EventEmitter();
  invalidAcc: boolean = false;
  invalidPin: boolean = false;
  constructor(
    private fb: FormBuilder,
    private shared: AppSharedService,
    private service: GiftCardService
  ) {
    this.model = new BalancePayload();
    this.model.reactiveForm = this.fb.group({
      phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(14)]],
      redeem_pin: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
    });
  }

  ngOnInit() {
  }

  get f(): any {
    return this.model.reactiveForm.controls;
  }

  onCodeCompleted(code: string) {
    this.model.reactiveForm.get('redeem_pin').setValue(code);
    this.model.reactiveForm.get('redeem_pin').markAsTouched();
    this.model.reactiveForm.get('redeem_pin').updateValueAndValidity();
    this.model.complete_pin = this.model.reactiveForm.get('redeem_pin').value;
    this.invalidPin = false;
    if (this.model.complete_pin.length == 4) {
      setTimeout(() => {
        const x = document.getElementById('checkbalancebtn');
        x.focus();
      }, 50);
    }

  }

  async save(): Promise<void> {
    await this.shared.delay(200);
    if (this.model.reactiveForm.invalid) {
      for (var i in this.f) {
        this.f[i].markAsTouched();
      }
      return;
    }

    this.isLoader = true;
    this.invalidAcc = false;
    this.invalidPin = false;
    const postData = {
      task: 'getappuserbalance',
      countryIsd: this.model.phone.dialCode,
      accountNumber: this.model.phone.nationalNumber.replace(/\s+/g, ''),
      pin: this.model.complete_pin
    }

    this.service.getUserBalance(postData.task, postData).subscribe(res => {
      if (res && res.Status == "Error") {
        if (res.Message.includes('Account not registered')) this.invalidAcc = true;
        if (res.Message.includes('Invalid account pin')) this.invalidPin = true;
      } else {
        this.onGetData.emit(res.Result);
        this.phonenumber.emit(this.model.phone.nationalNumber.replace(/\s+/g, ''));
      }
      this.isLoader = false;
    }, err => {
      this.invalidAcc = false;
      this.invalidPin = false;
      this.isLoader = false;
    })


  }
}
