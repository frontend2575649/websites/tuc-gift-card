import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-balance',
  templateUrl: './show-balance.component.html',
  styleUrls: ['./show-balance.component.scss']
})
export class ShowBalanceComponent implements OnInit {

  @Input() balance: any = null;
  @Input() number: any = null;
  
  constructor() { }

  ngOnInit() {
  }

}
