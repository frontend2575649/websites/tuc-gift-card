import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-balance-check-card',
  templateUrl: './balance-check-card.component.html',
  styleUrls: ['./balance-check-card.component.scss']
})
export class BalanceCheckCardComponent implements OnInit {

  balance_data: any = '';
  phoneNumber: any = '';

  constructor() { }

  ngOnInit() {
  }

  onGetData($event): void{
    this.balance_data = $event;
  }

  onGetPhoneNumber($event): void{
    this.phoneNumber = $event;
  }
}
