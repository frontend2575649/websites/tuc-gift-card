import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceCheckCardComponent } from './balance-check-card.component';

describe('BalanceCheckCardComponent', () => {
  let component: BalanceCheckCardComponent;
  let fixture: ComponentFixture<BalanceCheckCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceCheckCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceCheckCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
