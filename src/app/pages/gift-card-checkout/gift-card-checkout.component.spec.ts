import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftCardCheckoutComponent } from './gift-card-checkout.component';

describe('GiftCardCheckoutComponent', () => {
  let component: GiftCardCheckoutComponent;
  let fixture: ComponentFixture<GiftCardCheckoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GiftCardCheckoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCardCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
