import { NotifyService } from './../../shared/notify.service';
import { GiftCardService } from './../../shared/gift-card.service';
import { AppSharedService } from './../../configs/app-config/app-shared.service';
import { GiftCard } from './../../shared/gift_card';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { GiftCardPaymentComponent } from 'src/app/components/dialogs/gift-card-payment/gift-card-payment.component';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-gift-card-checkout',
  templateUrl: './gift-card-checkout.component.html',
  styleUrls: ['./gift-card-checkout.component.scss']
})
export class GiftCardCheckoutComponent implements OnInit {

  editType: string = '';
  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  public TransactionId = '';
  public PayStackKey = '';
  public email = "";
  model: GiftCard = new GiftCard();
  docFile!: File;
  reactiveForm!: FormGroup;
  checkoutDisabled: boolean = false;

  constructor(
    private dialog: MatDialog,
    private _Router: Router,
    private shared: AppSharedService,
    private fb: FormBuilder,
    private service: GiftCardService,
    private notify: NotifyService
  ) {
    this.reactiveForm = this.fb.group({
      amount: ['', [Validators.required, Validators.min(1000)]],
    });
  }

  ngOnInit(): void {
    if (!localStorage.getItem('tucgiftcarddetails')) {
      // this.notify.NotifyError('Something went wrong. Please try again!');
      this._Router.navigateByUrl('/');
    } else {
      this.model = JSON.parse(localStorage.getItem('tucgiftcarddetails'));
      if (!this.model.senderInfo) {
        this._Router.navigateByUrl('/');
        return;
      }
      this.f.amount.value = this.model.amount;
      this.email = this.model.senderInfo.Email;
      if (this.model.document_file) this.docFile = this.dataURLtoFile(this.model.document_file, 'GiftCard.xlsx');
      this.createTransaction();
    }
  }

  createTransaction(): void {
    const percente = (this.model.commission / 100) * Number(this.model.amount);
    this._AccountBalanceCreditAmount = Number(this.model.amount) + Math.round(percente);
    this.PayStackKey = environment.appSettings.PayStackKey;
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Tp' + '_' + Ref;
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }

  openPaymentOption() {
    if (Number(this.model.amount) < 1) {
      this.notify.NotifyError('Amount should be greater than ₦1000');
      return;
    }
    let dialogRef = this.dialog.open(GiftCardPaymentComponent, {
      width: '600px',
      height: '300px',
      maxWidth: '600px',
      maxHeight: '400px',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res == 'paysatck') {
        document.getElementById('paystackCheckout').click()
      }
    });
  }

  paymentDone(ref: any) {
    this.TransactionId = ref.trans;
    if (ref && ref.status == 'success') {
      this.checkoutPayment();
    }
    else {
      this.notify.NotifyError('Payment failed.');
    }
  }

  checkoutPayment() {
    this.checkoutDisabled = true;
    let start = new Date();
    let StartDate = new Date().toISOString().split('T')[0] + ' 00:00:00';
    let endDate = [start.getFullYear() + 1, (start.getMonth() < 10 ? '0' : '') + start.getMonth(), start.getDate()].join('-') + ' 00:00:00';
    var postdata = {
      Task: 'creategiftcard',
      Amount: this.model.amount,
      isIndividualorCorporate: this.model.main_type == "individual" ? 1 : 0, //0:Co-orporate,1:Individual
      type: this.model.type == "1" ? 1 : 0,  //0: single, 1:multiple
      senderInfo: this.model.senderInfo,
      receiverInfo: this.model.receiverInfo,
      StartDate: StartDate,
      EndDate: endDate,
      PaymentReference: this._TransactionReference,
      PaymentSource: "PayStack",
      TransactionId: this.TransactionId,
      CompanyName: this.model.company_name,
      Commission: this.model.commission
    }
    this.service.checkoutGiftCard(postdata.Task, postdata).subscribe(res => {
      this.checkoutDisabled = false;
      if (res) {
        this._Router.navigateByUrl(`/${this.shared.activateTemplate}/success`);
        localStorage.removeItem('tucgiftcarddetails');
      }
    }, err => {
      this.createTransaction();
      this.checkoutDisabled = false;
    })
  }

  paymentCancel() {
    this.notify.NotifyError('Payment has been cancelled.');
  }

  goToReciever(): void {
    if (this.model.type == '1' && this.model.main_type == 'individual' && this.model.no_receivers < 6) {
      this._Router.navigateByUrl(`/${this.model!.main_type}/receivers`);
    }
    else if (this.model!.type == '1') {
      this._Router.navigateByUrl(`/${this.model!.main_type}/giftcardupload`);
    } else {
      this._Router.navigateByUrl(`/${this.model!.main_type}/receiver`);
    }
  }

  goToSender(): void {
    this._Router.navigateByUrl(`/${this.model!.main_type}/sender`);
  }

  async cancelProcess(): Promise<void> {
    const swal = await Swal.fire({
      text: 'Do you want to cancel this process?',
      showConfirmButton: true,
      confirmButtonText: 'Confirm',
      confirmButtonColor: '#562A83',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
    });
    if (swal.value == true) {
      this._Router.navigateByUrl('/');
      localStorage.clear();
    }
  }

  downloadFile(): void {
    this.shared.download(this.docFile);
  }

  get f(): any {
    return this.reactiveForm.controls;
  }

  saveAmount(): void {
    if (this.reactiveForm.invalid) {
      for (var i in this.f) {
        this.f[i].markAsTouched();
      }
      return;
    }
    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data) {
      data.amount = this.f.amount.value;
      data.receiverInfo[0].Amount = Number(data.amount);
      localStorage.setItem('tucgiftcarddetails', JSON.stringify(data));
      this.model = data;
      this.createTransaction();
      this.editType = '';
    }
  }
}