import { GiftCardService } from './../../shared/gift-card.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gift-card',
  templateUrl: './gift-card.component.html',
  styleUrls: ['./gift-card.component.scss']
})
export class GiftCardComponent implements OnInit {

  constructor(private service: GiftCardService) { 
    localStorage.removeItem('tucgiftcarddetails')
  }

  ngOnInit(): void {
  }

}
