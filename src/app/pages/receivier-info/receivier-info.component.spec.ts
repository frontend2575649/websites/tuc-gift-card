import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivierInfoComponent } from './receivier-info.component';

describe('ReceivierInfoComponent', () => {
  let component: ReceivierInfoComponent;
  let fixture: ComponentFixture<ReceivierInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceivierInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivierInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
