import { AppSharedService } from 'src/app/configs/app-config/app-shared.service';
import { Router } from '@angular/router';
import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GiftCard } from 'src/app/shared/gift_card';
import utils from '../../../../node_modules/intl-tel-input/build/js/utils.js';
@Component({
  selector: 'app-receivier-info',
  templateUrl: './receivier-info.component.html',
  styleUrls: ['./receivier-info.component.scss']
})
export class ReceivierInfoComponent implements OnInit, AfterViewInit {

  reactiveForm!: FormGroup;
  amount: number = 0;
  phoneData: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private shared: AppSharedService,
    private cdr: ChangeDetectorRef
  ) {
    this.reactiveForm = this.fb.group({
      name: ['', Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
      // phone: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14), Validators.pattern("^((\\+243-?)|0)?[0-9]{10,14}$")])],
      phone: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
    });
  }

  ngAfterViewInit(): void {
    this.shared.scrollToView('scrollToCardDetails');
  }

  ngOnInit(): void {
    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data && data.receiverInfo && data.receiverInfo.length == 1) {
      this.f.name.value = data.receiverInfo[0].Name;
      this.f.email.value = data.receiverInfo[0].Email;
      this.f.phone.value = data.receiverInfo[0].MobileNo;
      this.f.phone.value = [( "+" + data.receiverInfo[0].CountryIsd), data.receiverInfo[0].MobileNo].join('');
    }
    if (data) {
      this.amount = Number(data.amount);
    }
  }

  get f(): any {
    return this.reactiveForm.controls;
  }

  onBlur(): void {
    this.reactiveForm.get('phone').markAsTouched();
  }

  save(): void {
    if (this.reactiveForm.invalid) {
      for (var i in this.f) {
        this.f[i].markAsTouched();
      }
      return;
    }
    let data: GiftCard = JSON.parse(localStorage.getItem('tucgiftcarddetails')) as GiftCard;
    if (data) {
      const receiver = {
        Name: this.f.name.value,
        Email: this.shared.lowerCaseConvert(this.f.email.value),
        MobileNo: this.phoneData.nationalNumber.replace(/\s+/g, ''),
        CountryIsd: this.phoneData.dialCode,
        Amount: Number(data.amount),
        Description: ''
      }
      data.receiverInfo = [{ ...receiver }];
      localStorage.setItem('tucgiftcarddetails', JSON.stringify(data));
    }
    this.router.navigateByUrl('/individual/sender');
  }

  back() {
    this.router.navigateByUrl('/individual/giftcarddetails');
  }
}
