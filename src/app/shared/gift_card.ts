import { DropDownConfig } from "search-multiselect-dropdown";

export class GiftCard {
    main_type: string = '';
    type: string = '';
    company_name: string = '';
    amount: string = '';
    senderInfo: SenderInformation;
    receiverInfo: ReceiverInformation[] = [];
    document_file: any = null;
    no_receivers: number = 0;
    is_equaly_divide: boolean = false;
    commission: number = 0;
}


export class GiftCardCheckout {
    Task: string = 'createquickgiftcard';
    Amount: number = 0;
    isIndividual: number = 0; //0:false,1:true
    isCoorportate: number = 0; //0:false,1:true
    type: number = 0; //0: single, 1:multiple
    senderInfo: SenderInformation;
    receiverInfo: ReceiverInformation[] = [];
    StartDate: Date = new Date();
    EndDate: Date = new Date();
    PaymentReference: string = ''
    PaymentSource: string = 'PayStack';
    TransactionId: string = '';
}

interface SenderInformation {
    Name: string,
    Email: string,
    MobileNo: string,
    CountryIsd: string,
    Description: string
}

interface ReceiverInformation {
    Name: string,
    Email: string,
    MobileNo: string,
    CountryIsd: string,
    Amount: number,
    Description: string
}


export const Selectconfig: DropDownConfig = {
    displayKey: 'text',
    search: false,
    height: '300px',
    placeholder: 'Search and select',
    noResultsFound: 'No result found',
    multiple: false,
    disabled: false,
    theme: {
        inputBackground: '#F8F8F8',
        inputColor: '#000',
        containerListBackground: '#F8F8F8',
        ContainerListColor: '#000',
        selectedItemColor: '#fff',
        selectedItemBackground: '#562A83',
        listHoverBackground: '#562A83',
        listHoverColor: '#fff',
        searchInputColor: '#fff'
    }
};

export const GiftCardTypes = [
    {
        id: '0',
        text: 'Single'
    },
    {
        id: '1',
        text: 'Multiple'
    },
];

export const CKEditorConfig = {
    toolbar: [
      // { name: 'basicstyles', items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },
      // { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blackquote'] },
      // { name: 'styles', items: ['Styles','Format'] }
    ]
  }
  