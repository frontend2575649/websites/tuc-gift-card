import { FormGroup } from '@angular/forms';
export class BalancePayload {
    reactiveForm: FormGroup;
    phone: any = '';
    pin1: string;
    pin2: string;
    pin3: string;
    pin4: string;
    complete_pin: string = '';
}