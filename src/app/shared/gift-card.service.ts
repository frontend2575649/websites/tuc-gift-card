import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpService } from '../configs/http-config/http.service';

@Injectable({
  providedIn: 'root'
})
export class GiftCardService {

  constructor(private httpService: HttpService) { }

  checkoutGiftCard(task: string, postData: Object): Observable<any> {
    const obj: any = {};
    obj.url = environment.appSettings.baseURI + '/api/v3/plugins/gc/' + task;
    obj.postData = postData;
    obj.is_toast = true;
    return this.httpService.post(obj);
  }


  getUserBalance(task: string, postData: Object): Observable<any> {
    const obj: any = {};
    obj.url = environment.appSettings.baseURI + '/api/v3/plugins/gc/' + task;
    obj.postData = postData;
    obj.is_toast = false;
    obj.isAuth = false;
    return this.httpService.post(obj);
  }

  saveGiftDraft(task: string, postData: Object): Observable<any> {
    const obj: any = {};
    obj.url = environment.appSettings.baseURI + '/api/v3/plugins/gc/' + task;
    obj.postData = postData;
    obj.is_toast = false;
    return this.httpService.post(obj);
  }

}
