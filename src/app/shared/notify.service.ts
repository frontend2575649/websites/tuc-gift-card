import { Injectable } from '@angular/core';
import notie from "notie";

@Injectable({
    providedIn: 'root'
})
export class NotifyService {

    constructor() {
    }

    NotifyInfo(Message: string) {
        notie.alert({
            type: "4",
            text: Message,
            time: 5
        });
    }
    NotifySuccess(Message: string) {
        notie.alert({
            type: "1",
            text: Message,
            time: 5
        });
    }
    NotifyWarning(Message: string) {
        notie.alert({
            type: "2",
            text: Message,
            time: 5
        });
    }
    NotifyError(Message: string) {
        notie.alert({
            type: "3",
            text: Message,
            time: 8
        });
    }
}