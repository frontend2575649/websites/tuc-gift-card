import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftCardImageComponent } from './gift-card-image.component';

describe('GiftCardImageComponent', () => {
  let component: GiftCardImageComponent;
  let fixture: ComponentFixture<GiftCardImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GiftCardImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCardImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
