import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-gift-card-image',
  templateUrl: './gift-card-image.component.html',
  styleUrls: ['./gift-card-image.component.scss']
})
export class GiftCardImageComponent implements OnInit {

  date: string = '';
  @Input() amount: number = 0;

  constructor() {
    this.date = moment(new Date()).add(1, 'y').format('Do MMM, YYYY');
  }

  ngOnInit(): void {
  }

  getAmount(amount: any): number {
    return amount ? Number(amount) : 0;
  }

}
