import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GiftCardImageComponent } from './gift-card-image/gift-card-image.component';
import { GiftCardTypesComponent } from './gift-card-types/gift-card-types.component';
import { DragDropUploadDirective } from './directives/drag-upload.directive';
import { GiftCardPaymentComponent } from './dialogs/gift-card-payment/gift-card-payment.component';
import { ErrorMesssagesComponent } from './error-messsages/error-messsages.component';
import { PhoneNumberComponent } from './phone-number/phone-number.component';
import { DecimalDirective } from './directives/decimal.derective';
import { IntergerDirective } from './directives/interger.derective';

@NgModule({
  declarations: [
    GiftCardImageComponent,
    GiftCardTypesComponent,
    DragDropUploadDirective,
    GiftCardPaymentComponent,
    ErrorMesssagesComponent,
    PhoneNumberComponent,
    DecimalDirective,
    IntergerDirective
  ],
  exports: [
    GiftCardImageComponent,
    GiftCardTypesComponent,
    DragDropUploadDirective,
    GiftCardPaymentComponent,
    ErrorMesssagesComponent,
    PhoneNumberComponent,
    DecimalDirective,
    IntergerDirective
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  entryComponents:[
    GiftCardPaymentComponent
  ]
})
export class ComponentsModule { }
