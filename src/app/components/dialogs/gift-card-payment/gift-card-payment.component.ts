import { Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
import { MatDialogRef} from '@angular/material';


@Component({
  selector: 'app-gift-card-payment',
  templateUrl: './gift-card-payment.component.html',
  styleUrls: ['./gift-card-payment.component.scss']
})
export class GiftCardPaymentComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<GiftCardPaymentComponent>,
  ) { }

  ngOnInit() {
    Feather.replace();   
  }

  
  close(data:any)
  {   
    this.dialogRef.close(data)
  }
}
