import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';
@Directive({
    selector: '[appDragDrop]'
})
export class DragDropUploadDirective {

    @Output() onFileDropped = new EventEmitter<any>();
    constructor() { }

    @HostBinding('style.background - color') public background = '#fff';
    @HostBinding('style.opacity') public opacity = '1';

    @HostListener('dragover', ['$event']) onDragOver(evt: any) {
        evt.preventDefault();
        evt.stopPropagation();
        this.background = '#9ecbec';
        this.opacity = '0.8'
    }

    @HostListener('dragleave', ['$event']) public onDragLeave(evt: any) {
        evt.preventDefault();
        evt.stopPropagation();
        this.background = '#fff'
        this.opacity = '1'
    }

    @HostListener('drop', ['$event']) public ondrop(evt: any) {
        evt.preventDefault();
        evt.stopPropagation();
        this.background = '#f5fcff'
        this.opacity = '1'
        let files = evt.dataTransfer.files;
        if (files.length > 0) {
            this.onFileDropped.emit(files)
        }
    }
}