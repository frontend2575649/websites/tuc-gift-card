import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftCardTypesComponent } from './gift-card-types.component';

describe('GiftCardTypesComponent', () => {
  let component: GiftCardTypesComponent;
  let fixture: ComponentFixture<GiftCardTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GiftCardTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCardTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
