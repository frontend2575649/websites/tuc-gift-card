import { AppSharedService } from './../../configs/app-config/app-shared.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GiftCard } from 'src/app/shared/gift_card';

@Component({
  selector: 'app-gift-card-types',
  templateUrl: './gift-card-types.component.html',
  styleUrls: ['./gift-card-types.component.scss']
})
export class GiftCardTypesComponent implements OnInit {

  model: GiftCard;

  constructor(
    private router: Router,
    public shared: AppSharedService
  ) {
    this.model = new GiftCard();
  }

  ngOnInit(): void {
  }

  goToIndividual(): void {
    this.model = new GiftCard();
    this.model.main_type = 'individual'
    localStorage.setItem('tucgiftcarddetails', JSON.stringify(this.model))
    this.router.navigateByUrl('/individual');
  }

  goToCorporate(): void {
    this.model = new GiftCard();
    this.model.main_type = 'corporate'
    localStorage.setItem('tucgiftcarddetails', JSON.stringify(this.model))
    this.router.navigateByUrl('/corporate');
  }
}
