import { Component, forwardRef, Input, AfterViewInit, ChangeDetectorRef, EventEmitter, Output, OnInit } from '@angular/core';
import * as lpn from 'google-libphonenumber';
import * as utils from '../../../../node_modules/intl-tel-input/build/js/utils.js';
import intlTelInput from 'intl-tel-input';
import { ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

export let iti: any;

export const phoneNumberValidator = (control: any) => {
  if (!control.value) {
    return;
  }
  // console.log(iti.getNumber());

  if (iti.getNumber()) {
    var phoneUtil = lpn.PhoneNumberUtil.getInstance();
    // console.log(phoneUtil.parseAndKeepRawInput(iti.getNumber()));
    // console.log(phoneUtil.isValidNumber(phoneUtil.parseAndKeepRawInput(iti.getNumber())));
    if (!phoneUtil.isValidNumber(phoneUtil.parseAndKeepRawInput(iti.getNumber()))) {
      return { invalid: true };
    }
  }
  return;
};


@Component({
  selector: 'app-phone-number',
  templateUrl: './phone-number.component.html',
  styleUrls: ['./phone-number.component.scss'],
  host: { '(blur)': 'onTouched($event)' },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PhoneNumberComponent),
      multi: true,
    },
    // {
    //   provide: NG_VALIDATORS,
    //   useValue: phoneNumberValidator,
    //   multi: true,
    // },
  ],
})
export class PhoneNumberComponent implements OnInit, AfterViewInit, ControlValueAccessor {
  phoneNumber: any = '';
  phoneUtil: any = lpn.PhoneNumberUtil.getInstance();
  selectedCountry: any;
  disabled = false;
  inputID: string = this.getInputID();
  @Input() class: string = '';
  @Output() getPhoneData: EventEmitter<Phone> = new EventEmitter();
  @Output() blur: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngAfterViewInit(): void {

  }

  getInputID(): string {
    return Math.random().toString(36).substring(2, 22 + 2);
  }

  onTouched = () => { };
  propagateChange = (_: Phone) => { };
  registerOnChange(fn: any): void { this.propagateChange = fn; }
  registerOnTouched(fn: any) { this.onTouched = fn; }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public onInputKeyPress(event: KeyboardEvent): void {
    const allowedChars = /[0-9\+\-\(\)\ ]/;
    const allowedCtrlChars = /[axcv]/; // Allows copy-pasting
    const allowedOtherKeys = [
      'ArrowLeft',
      'ArrowUp',
      'ArrowRight',
      'ArrowDown',
      'Home',
      'End',
      'Insert',
      'Delete',
      'Backspace',
    ];

    if (
      !allowedChars.test(event.key) &&
      !(event.ctrlKey && allowedCtrlChars.test(event.key)) &&
      !allowedOtherKeys.includes(event.key)
    ) {
      event.preventDefault();
    }
  }

  writeValue(obj: any): void {
    this.phoneNumber = obj;
    setTimeout(() => {
      iti.setNumber(iti.getNumber());
      this.onPhoneNumberChange();
    }, 100);
  }

  init() {
    const input = document.querySelector('#telInputPhone');
    iti = intlTelInput(input, {
      onlyCountries: ["ng"],
      initialCountry: 'ng',
      separateDialCode: true,
      utilsScript: utils,
      hiddenInput: "full_phone",
    });
    this.selectedCountry = iti.getSelectedCountryData();
  }


  public onPhoneNumberChange(): void {
    let countryCode: string | undefined;
    if (this.phoneNumber && typeof this.phoneNumber === 'object') {
      const numberObj: Phone = this.phoneNumber;
      this.phoneNumber = numberObj.number;
      countryCode = numberObj.countryCode;
    }
    this.selectedCountry = iti.getSelectedCountryData();
    countryCode = countryCode || this.selectedCountry.iso2;
    const number = this.getParsedNumber(this.phoneNumber, countryCode);
    if (number) {
      this.propagateChange(number.values_['2'].toString());
      var elNo: any = number ? this.phoneUtil.format(number, lpn.PhoneNumberFormat.E164) : '';
      if (elNo) elNo = elNo.replace('+', '');
      const intlNo = number
        ? this.phoneUtil.format(number, lpn.PhoneNumberFormat.INTERNATIONAL)
        : '';
      this.getPhoneData.emit({
        number: this.phoneNumber,
        internationalNumber: intlNo,
        nationalNumber: number
          ? this.phoneUtil.format(number, lpn.PhoneNumberFormat.NATIONAL)
          : '',
        e164Number: number
          ? this.phoneUtil.format(number, lpn.PhoneNumberFormat.E164)
          : '',
        countryCode: countryCode.toUpperCase(),
        dialCode: this.selectedCountry.dialCode,
        dialCodeWithPlus: "+" + this.selectedCountry.dialCode,
      })

    } else {
      this.propagateChange(null);
    }
  }

  onChangeNumber(): void {
    // iti.setNumber(iti.getNumber());
    this.blur.emit(true);
  }

  private getParsedNumber(
    phoneNumber: string,
    countryCode: string
  ): lpn.PhoneNumber {
    let number: lpn.PhoneNumber;
    try {
      number = this.phoneUtil.parse(phoneNumber, countryCode.toUpperCase());
    } catch (e) { }
    return number;
  }

}

interface Phone {
  number?: string;
  internationalNumber?: string;
  nationalNumber?: string;
  e164Number?: string;
  countryCode?: string;
  dialCode?: string;
  dialCodeWithPlus?: string;
}
