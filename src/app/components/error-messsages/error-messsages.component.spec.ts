import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorMesssagesComponent } from './error-messsages.component';

describe('ErrorMesssagesComponent', () => {
  let component: ErrorMesssagesComponent;
  let fixture: ComponentFixture<ErrorMesssagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorMesssagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorMesssagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
