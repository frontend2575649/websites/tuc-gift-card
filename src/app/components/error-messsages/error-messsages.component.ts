import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-error-messsages',
  templateUrl: './error-messsages.component.html',
  styleUrls: ['./error-messsages.component.scss']
})
export class ErrorMesssagesComponent {

  @Input() form: any;
  @Input() required: string = 'The field is required.';
  @Input() pattern: string = '';
  @Input() notEqual: string = '';
  @Input() validatePhoneNumber: string = 'Invalid mobile number!';
  @Input() ConfirmedValidator: string = 'Password Not Match';
  @Input() email: string = 'Email is not valid.';
  @Input() min: string = '';
  @Input() max: string = '';
  @Input() invalid: string = '';

  constructor() { }

  ngOnInit(): void {
  }


}
